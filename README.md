# openvino-imx8-hbpulse

OpenVINO Inference Framework Setup on IMX8 Hummingboard Pulse (SolidRun).

# OpenVINO Setup
Install OpenVINO on SolidRun Hummingboard Pulse under Debian 10 OS.

# Install OpenVINO Toolkit
@ref: https://software.intel.com/en-us/articles/intel-neural-compute-stick-2-and-open-source-openvino-toolkit

There are two ways to get OpenVINO platform installed:
* Intel Distribution of OpenVINO toolkit (include HDDL plugin)
* Manualy compilation from OpenCV/dldt repo (include just MYRIAD plugin)

The following uses manual compilation since the Intel distribution setup script doesn't tailored for Debian OS.

## Requirements
* make 3.14.3 or above
* opencv
* python 3.6 or above

### Build make
Debian come with cmake with older version, so manual compile is needed.
```
wget https://github.com/Kitware/CMake/releases/download/v3.14.3/cmake-3.14.3.tar.gz
tar xvf cmake-3.14.3.tar.gz
cd cmake-3.14.3
./bootstrap
make
sudo make install
```

After building and installing cmake, any open terminals may need to be closed and re-opened to use the newly installed cmake.

### Build OpenCV
@ref: https://docs.opencv.org/master/d7/d9f/tutorial_linux_install.html

Install ffpmeg first before building OpenCV so that it uses ffmpeg library and can read video files as input (which is used in our later OpenVINO-Yolo example)
```
sudo apt install ffmpeg
```

Without ffmpeg installed before OpenCV build, the following error will occur when using `OpenCV Video Capture` trying to read a video file.
```
Unable to stop the stream: Inappropriate ioctl for device
```

Steps

```
mkdir ~/build-opencv && cd ~/build-opencv
wget https://github.com/opencv/opencv/archive/4.1.1.zip
unzip 4.1.1.zip
cd 4.1.1
mkdir build && cd build
cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=/usr/local ..
make -j4
sudo make install
```

output
* Notice the detected FFMPEG
```
...
--     FFMPEG:                      YES
--       avcodec:                   YES (58.35.100)
--       avformat:                  YES (58.20.100)
--       avutil:                    YES (56.22.100)
--       swscale:                   YES (5.3.100)
--       avresample:                NO
--     GStreamer:                   NO
--     v4l/v4l2:                    YES (linux/videodev2.h)
--
--   Parallel framework:            pthreads
--
--   Trace:                         YES (with Intel ITT)
--
--   Other third-party libraries:
--     Lapack:                      NO
--     Eigen:                       NO
--     Custom HAL:                  YES (carotene (ver 0.0.1))
--     Protobuf:                    build (3.5.1)
--
--   OpenCL:                        YES (no extra features)
--     Include path:                /home/gary/build-opencv/opencv/3rdparty/include/opencl/1.2
--     Link libraries:              Dynamic load
--
--   Python 3:
--     Interpreter:                 /usr/bin/python3 (ver 3.7.3)
--     Libraries:                   /usr/lib/aarch64-linux-gnu/libpython3.7m.so (ver 3.7.3)
--     numpy:                       /home/gary/.local/lib/python3.7/site-packages/numpy/core/include (ver 1.17.2)
--     install path:                lib/python3.7/dist-packages/cv2/python-3.7
--
--   Python (for build):            /usr/bin/python2.7
--
--   Java:
--     ant:                         NO
--     JNI:                         NO
--     Java wrappers:               NO
--     Java tests:                  NO
--
--   Install to:                    /usr/local

```



### Build Python
Update python to use at least 3.6. The current Debian 10 has both 2.7. and 3.7 installed. Use `update-alternatives` to update the symlink systemwide. The higher integer at the end denotes higher priority.
```
# update-alternatives --install /usr/bin/python python /usr/bin/python2.7 1
update-alternatives: using /usr/bin/python2.7 to provide /usr/bin/python (python) in auto mode
# update-alternatives --install /usr/bin/python python /usr/bin/python3.7 2
update-alternatives: using /usr/bin/python3.7 to provide /usr/bin/python (python) in auto mode
```

Check python version is switched correct
```
python --version
```



## Build OpenVINO Toolkit
```
mkdir ~/build-openvino && cd ~/build-openvino
git clone https://github.com/opencv/dldt.git
cd dldt/inference-engine
git submodule init
git submodule update --recursive
```

Git submodule will pull source files from other referenced repo.

### Dependencies
```
sudo apt-get install libssl-dev \
            curl \
            wget \
            libssl-dev \
            ca-certificates \
            git \
            libboost-regex-dev \
            libgtk2.0-dev \
            pkg-config \
            unzip \
            automake \
            libtool \
            autoconf \
            libcairo2-dev \
            libpango1.0-dev \
            libglib2.0-dev \
            libgtk2.0-dev \
            libswscale-dev \
            libavcodec-dev \
            libavformat-dev \
            libgstreamer1.0-0 \
            gstreamer1.0-plugins-base \
            libusb-1.0-0-dev \
            libopenblas-dev \
			libpng-dev \
			ffmpeg
```

### Build
```
export OpenCV_DIR=/usr/local/share/opencv4
cd ~/dldt/inference-engine
mkdir build && cd build
cmake -DCMAKE_BUILD_TYPE=Release \
	-DENABLE_MKL_DNN=OFF \
	-DENABLE_CLDNN=OFF \
	-DENABLE_GNA=OFF \
	-DENABLE_SSE42=OFF \
	-DTHREADING=SEQ \
	..
make
```

The binaries samples are found at `dldt/inference-engine/bin/aarch64/Release`

The libraries is found at `dldt/inference-engine/bin/aarch64/Release/lib`

output example
```
gary@sr-imx8:~/build-openvino/dldt/inference-engine/bin/aarch64/Release$ ls -alh
total 3.2M
drwxr-xr-x 3 gary gary 4.0K Sep 24 19:13 .
drwxr-xr-x 3 gary gary 4.0K Sep 19 16:17 ..
-rwxr-xr-x 1 gary gary 343K Sep 24 19:10 benchmark_app
-rwxr-xr-x 1 gary gary 219K Sep 24 19:10 classification_sample_async
-rwxr-xr-x 1 gary gary 103K Sep 24 19:10 hello_classification
-rwxr-xr-x 1 gary gary  99K Sep 24 19:11 hello_nv12_input_classification
-rwxr-xr-x 1 gary gary  51K Sep 24 19:11 hello_query_device
-rwxr-xr-x 1 gary gary  91K Sep 24 19:12 hello_reshape_ssd
drwxr-xr-x 3 gary gary 4.0K Sep 24 19:08 lib
-rwxr-xr-x 1 gary gary 207K Sep 19 18:35 myriad_compile
-rwxr-xr-x 1 gary gary 203K Sep 24 19:12 object_detection_sample_ssd
-rw-r--r-- 1 gary gary 901K Sep 24 17:06 out_0.bmp
-rwxr-xr-x 1 gary gary  178 Sep 20 17:43 run_benchmark_app.sh
-rwxr-xr-x 1 gary gary  144 Sep 23 16:00 run_object_detection_sample_ssd.sh
-rwxr-xr-x 1 gary gary 215K Sep 24 19:13 speech_sample
-rwxr-xr-x 1 gary gary 311K Sep 19 18:34 statistics_collector
-rwxr-xr-x 1 gary gary 199K Sep 24 19:08 style_transfer_sample
-rwxr-xr-x 1 gary gary 219K Sep 19 18:35 vpu_profile

```

Note:
If the `make` command fails because of an issue with an OpenCV library, make sure that you’ve told the system where your installation of OpenCV via the `export OpenCV_DIR` flag. The path should contains the following:
```
gary@sr-imx8:~/build-openvino/dldt/inference-engine/bin/aarch64/Release$ cd /usr/local/share/opencv4/
gary@sr-imx8:/usr/local/share/opencv4$ ls -alh
total 24K
drwxr-xr-x 4 root root 4.0K Sep 19 15:55 .
drwxr-xr-x 9 root root 4.0K Sep 19 15:55 ..
drwxr-xr-x 2 root root 4.0K Sep 19 15:55 haarcascades
drwxr-xr-x 2 root root 4.0K Sep 19 15:55 lbpcascades
-rw-r--r-- 1 root root 2.6K Sep 18 19:01 valgrind_3rdparty.supp
-rw-r--r-- 1 root root 4.0K Sep 18 19:01 valgrind.supp
```

#### Troubleshooting
Error 1
```
pkg_resources.DistributionNotFound: The 'pip==10.0.1' distribution was not found and is required by the application
```
@ref: https://stackoverflow.com/questions/39845636/the-pip-7-1-0-distribution-was-not-found-and-is-required-by-the-application

Solution: Remove existing pip and download the 10.0.1.

```
sudo apt-purge python-pip
sudo easy_install pip==10.0.1
```

Error 2
```
libboost_thread.so.1.58.0: cannot open shared object file: No such file or directory

```
Solution:
```
sudo apt-get install libboost-dev libboost-all-dev
```

### Install Intel NCS2 Linux USB Driver
This driver is needed for the MYRIAD plugin to load the model into NCS2.

To check whether this driver is loaded during installation:
```
ls -alh /etc/udev/rules.d
total 16K
drwxr-xr-x 2 root root 4.0K Sep 23 17:09 .
drwxr-xr-x 4 root root 4.0K Mar  5  2019 ..
-rw-r--r-- 1 root root  505 Sep 23 17:09 97-myriad-usbboot.rules
```

If the file does not exist, do the following:

Add current user to the `users` group
```
sudo usermod –a –G users "$(whoami)"
```

Re-log to take effect
```
logout
```

Once logged in, verify that the user is added to the `users` group
```
groups
```

Add the USB `udev` rules so that any members of `users` group have access to this driver.
```
cp ~/build-openvino/dldt/inference-engine/thirdparty/movidius/mvnc/src/97-myriad-usbboot.rules /etc/udev/rules.d/97-myriad-usbboot.rules
udevadm control --reload-rules
udevadm trigger
ldconfig
```


---
# Download Open Model
Open Model Zoo include a model downloader to dowload open source models. It also contains demos to use the InferenceEngine.

## Requirements
```
pip install pyyaml
```

## Install Model Downloader
```
git clone https://github.com/opencv/open_model_zoo/
cd open_model_zoo/tools
python downloader.py --name <> name
```


---
# Run samples
All samples uses media downloaded from this repo.
```
mkdir ~/media && cd ~/media
git clone https://github.com/nealvis/media
```





1. Face Detection
@ref https://chtseng.wordpress.com/2019/01/21/intel-openvino%E4%BB%8B%E7%B4%B9%E5%8F%8A%E6%A8%B9%E8%8E%93%E6%B4%BE%E3%80%81linux%E7%9A%84%E5%AE%89%E8%A3%9D/

Download model
```
mkdir ~/models && cd ~/models
wget –no-check-certificate https://download.01.org/openvinotoolkit/2018_R4/open_model_zoo/face-detection-adas-0001/FP16/face-detection-adas-0001.bin
wget –no-check-certificate https://download.01.org/openvinotoolkit/2018_R4/open_model_zoo/face-detection-adas-0001/FP16/face-detection-adas-0001.xml
```

Run the sample
```
cd ~/build-openvino/dldt/inference-engine/bin/aarch64/Release
./object_detection_sample_ssd -m ~/models/face-detection-adas-0001.xml -d MYRIAD -i ~/media/custom/indias-most-populous-state.jpg

```

2. Benchmark app
```
cd ~/build-openvino/dldt/inference-engine/bin/aarch64/Release
./benchmark_app -i ~/media/media/face_pics/president_reagan-62x62.bmp -m ~/models/age-gender-recognition-retail-0013.xml ./lib -api async -d MYRIAD
```


---
# Build OpenVINO-YOLO
@ref: https://github.com/PINTO0309/OpenVINO-YoloV3/blob/master/cpp/README.md

The repo contains `convert_weights.py` to convert yolo weights into .pb that IR can parse.

```
git clone https://github.com/PINTO0309/OpenVINO-YoloV3
```

Copy the shared library and rules

```
sudo touch /etc/ld.so.conf.d/intel-openvino.conf
echo /opt/intel/openvino/lib/ >> /etc/ld.so.conf.d/intel-openvino.conf
```

Copy and link the shared library for OpenCV if the program complaint about shared library missing.

```
sudo touch /etc/ld.so.conf.d/opencv.conf
sudo echo /opt/intel/openvino_2019.2.275/opencv/lib/ >> /etc/ld.so.conf.d/opencv.conf
sudo ln -s libopencv_highgui.so.4.1.1 libopencv_highgui.so.4.0
sudo ln -s libopencv_videoio.so.4.1.1 libopencv_videoio.so.4.0
sudo ln -s libopencv_imgproc.so.4.1.1 libopencv_imgproc.so.4.0
sudo ln -s libopencv_core.so.4.1.1 libopencv_core.so.4.0
```



---
# Build InferenceEngine Demo
@ref: https://github.com/opencv/open_model_zoo/tree/master/demos

## Environment Setup
```
export InferenceEngine_DIR=/home/gary/build-openvino/dldt/inference-engine/build
export OpenCV_DIR=/usr/local/share/opencv4
export LD_LIBRARY_PATH="$LD_LIBRARY_PATH：/home/gary/build-openvino/dldt/inference-engine/bin/aarch64/Release/lib"
```

## Build
```
cd ~/open_model_zoo/demos
mkdir build && cd build
cmake -DCMAKE_BUILD_TYPE=Release ..
make
```

The binaries are located in `~/open_model_zoo/demos/aarch64`


## Prepare the Model
The model used are from Tiny-Yolov3 Model
@ref: https://docs.openvinotoolkit.org/latest/_docs_MO_DG_prepare_model_convert_model_tf_specific_Convert_YOLO_From_Tensorflow.html

Dump Yolov3 Tensorflow model
```
git clone https://github.com/mystic123/tensorflow-yolo-v3.git
cd tensorflow-yolo-v3
git checkout ed60b90
```

Download the labels from Darknet
```
wget https://raw.githubusercontent.com/pjreddie/darknet/master/data/coco.names
```

Download the weights
```
wget https://pjreddie.com/media/files/yolov3-tiny.weights
```

Convert the weights
The conversion will uses tensorflow Python module
```
python3 convert_weights_pb.py --class_names coco.names --data_format NHWC --weights_file yolov3-tiny.weights --tiny
```

output
```
923] Device interconnect StreamExecutor with strength 1 edge matrix:
2019-09-26 14:41:09.011699: I tensorflow/core/common_runtime/gpu/gpu_device.cc:929]      0
2019-09-26 14:41:09.011709: I tensorflow/core/common_runtime/gpu/gpu_device.cc:942] 0:   N
2019-09-26 14:41:09.012904: I tensorflow/core/common_runtime/gpu/gpu_device.cc:1053] Created TensorFlow device (/job:localhost/replica:0/task:0/device:GPU:0 with 10087 MB memory) -> physical GPU (device: 0, name: GeForce GTX 1080 Ti, pci bus id: 0000:01:00.0, compute capability: 6.1)
Converted 59 variables to const ops.
321 ops written to frozen_darknet_yolov3_model.pb.
```

If tensorflow module is not installed,
```
sudo apt install libhdf5-dev
pip install PIL
python3 -m pip install --upgrade https://github.com/lhelontra/tensorflow-on-arm/releases/download/v1.14.0-buster/tensorflow-1.14.0-cp37-none-linux_aarch64.whl
```

The list of other tensorflow version can be found here https://github.com/lhelontra/tensorflow-on-arm/releases

Convert yolov3 model into IR model
```
sudo pip install networkx test-generator difusedxml
cd ~/build-openvino/dldt/model-optimizer
python3 mo_tf.py
--input_model OpenVINO-YOLO/frozen_darknet_yolov3_model.pb
--tensorflow_use_custom_operations_config ~/build-openvino/dldt/extensions/front/tf/yolo_v3_tiny.json
--batch 1
```

output
```
[ SUCCESS ] Generated IR model.
[ SUCCESS ] XML file: /home/gary/build-openvino/dldt/model-optimizer/./frozen_darknet_yolov3_model.xml
[ SUCCESS ] BIN file: /home/gary/build-openvino/dldt/model-optimizer/./frozen_darknet_yolov3_model.bin
[ SUCCESS ] Total execution time: 81.97 seconds.
gary@sr-imx8:~/build-openvino/dldt/model-optimizer$ ls -alh
total 34M
drwxr-xr-x  6 gary gary 4.0K Sep 26 14:56 .
drwxr-xr-x  7 gary gary 4.0K Sep 20 18:37 ..
drwxr-xr-x  7 gary gary 4.0K Sep 26 14:55 extensions
-rw-r--r--  1 gary gary  34M Sep 26 14:56 frozen_darknet_yolov3_model.bin
-rw-r--r--  1 gary gary 5.6K Sep 26 14:56 frozen_darknet_yolov3_model.mapping
-rw-r--r--  1 gary gary  22K Sep 26 14:56 frozen_darknet_yolov3_model.xml
...
```

Copy the converted IR (.xml and .bin) to our a convenience folder that store our model.
```
mkdir -p ~/models/yolov3
cp frozen_darknet_yolov3* ~/models/yolov3/
```


## Run the Yolov3 Demo
```
cd ~/open_model_zoo/demos/build/aarch64/Release
./object_detection_demo_yolov3_async -i ~/media/media/traffic_vid/police_car_6095_shortened_960x540.mp4 -m ~/models/yolov3/frozen_darknet_yolov3_model.xml -d MYRIAD
```

Finger crossed that this will work!